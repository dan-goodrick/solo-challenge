import numpy as np
from plyfile import PlyData, PlyElement

np.set_printoptions(suppress=True)
np.set_printoptions(linewidth=400)

def open_ply(file_path):
    rdata = PlyData.read(file_path)
    points = []
    for i in range(len(rdata.elements[0].data)):
    # for i in range(0, len(rdata.elements[0].data), 100):
        point = rdata.elements[0].data[i]
        a = np.array(list(point))
        points.append(a)
    data = np.array(points)
    return data


def write_ply(name, data):
    tuples = []
    for point_i in range(data.shape[0]):
        tuples.append(tuple(data[point_i, :9]))

    described_data = np.array(
        tuples,
        dtype=[
            ("x", "double"),
            ("y", "double"),
            ("z", "double"),
            ("nx", "double"),
            ("ny", "double"),
            ("nz", "double"),
            ("red", "u1"),
            ("green", "u1"),
            ("blue", "u1"),
        ],
    )
    element = PlyElement.describe(described_data, "vertex")
    PlyData([element], text=False).write(name)

def get_color_mask(points, color):
    '''returns a rows of points where color matches columns 7-9'''
    return np.all(points[:,6:] == color, axis=1)

def get_normal_outliers(points, color_mask):
    '''returns a rows of all points where normal is more than 2 
        sigma from the mean normal of points with a particular color
    '''
    color_pts = points[color_mask, :]
    average = np.mean(color_pts[:,(3,4,5)], axis=0)
    std = np.std(color_pts[:,(3,4,5)], axis=0)
    return np.any(np.abs(points[:,(3,4,5)] - average)> 2 * std, axis=1)

def get_height_outliers(points, color_mask):
    '''returns rows of all points where the height is more than 2
       sigma from the mean height of points with a particular color
    '''
    color_pts = points[color_mask, :]
    average = np.mean(color_pts[:,2], axis=0)
    std = np.std(color_pts[:,2], axis=0)
    return np.abs(points[:,2] - average)> 2 * std

def main():
    points = open_ply("data/PointCloud.ply")
    colors = np.unique(points[:,6:], axis=0)[1:]
    for color in colors:
        color_mask = get_color_mask(points, color)
        normal_outliers = get_normal_outliers(points, color_mask)
        height_outliers = get_height_outliers(points, color_mask)
        mask = color_mask & (normal_outliers | height_outliers)
        points[mask, 6:] = 0
    write_ply("data/Result.ply", points)
    return 0


if __name__ == "__main__":
    main()
